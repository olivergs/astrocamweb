#!/bin/bash

if [ ! -d env ]; then
    echo "Creating virtual environment"
    virtualenv env -p python3
    echo "Activating virtual environment"
    source ./env/bin/activate
    echo "Installing python development dependencies"
    pip install -r requirements.txt
else
    echo "Activating virtual environment"
    source ./env/bin/activate
fi


source env/bin/activate
FLASK_APP=app.py FLASK_ENV=development flask run -h 0 -p 5000

// AstroCamWeb
// Author: Oliver Gutierrez <ogutsua@gmail.com>

var GIF_BASE64_PIXEL = 'data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=='

var FORCED_SETTINGS = {
    'drivemode': 'Single',
    'meteringmode': 'Evaluative',
    'aeb': 'off',
    'whitebalance': 'Daylight',
    'colorspace': 'sRGB',
    'picturestyle': 'Faithful',
    'imageformat': 'RAW + Large Fine JPEG', 
}

$(document).ready(function(){

    var config_elems = []

    // Populate configuration choices
    function populate_choices(option, choices, current) {
        var elem = $('#' + option)
        if (elem) {
            elem.html('');
            $.each(choices, function() {
                var opt = $("<option />").val(this).text(this);
                if(this == current) opt.attr('selected', true);
                elem.append(opt);
            });
        }
    }

    // Load camera configuration
    function load_config() {
        // TODO: Load camera info (cameramodel, batterylevel, lensname,
        //                         availableshots, shuttercounter, autopoweroff)
        console.log('Loading camera configuration')
        $.ajax({
            type: 'GET',
            url: '/config/',
            dataType: 'json',
            success: function(response) {
                console.log('Configuration read:', response)
                config_elems = [];
                $.each(response, function(name, elem) {
                    config_elems.push(name);
                    populate_choices(name, elem.choices, elem.current)
                });

            }
        });
    }

    // Set camera configuration
    function set_config(cb){
        console.log('Setting camera configuration')
        var data = {};

        $.each(config_elems, function() {
            data[this] = $('#' + this).val()
        });

        // Force forced settings if not in advanced mode
        if (!$('#advanced_checkbox').prop('checked')) {
            $.each(FORCED_SETTINGS, function(elem, value) {
                data[elem] = value
            });
        }

        console.log('Config to be set:', data)

        $.ajax({
            type: 'POST',
            url: '/config/',
            data: data,
            dataType: 'json',
            success: function success(response) {
                // Reload configuration
                load_config();
                if (typeof(cb) == 'function') cb();
            }
        });

    }

    // Capture an image and show it
    function capture_image() {
        // First apply current configuration
        set_config(function(){
            $('#action_buttons > button').prop('disabled', true);
        
            $.ajax({
                type: 'GET',
                url: '/image/capture/',
                //data: data,
                dataType: 'json',
                success: function(response) {
                    $('#action_buttons > button').prop('disabled', false);
                    $('#captured_image')
                        .attr('src', 'data:image/jpg;base64,' + response.imagedata)
                        $('#captured_image_modal').modal();
                }
            });
        });
    }

    // Capture an image using bulb timer and show it
    function capture_image_bulb() {
        // First apply current configuration
        set_config(function(){
            $('#action_buttons > button').prop('disabled', true);
        
            var data = {
                'seconds': parseInt($('#takeseconds').val()),
            }

            $.ajax({
                type: 'POST',
                url: '/image/capture_bulb/',
                data: data,
                dataType: 'json',
                success: function(response) {
                    $('#action_buttons > button').prop('disabled', false);
                    if (response.imagedata != null) {
                        $('#captured_image')
                            .attr('src', 'data:image/jpg;base64,' + response.imagedata)
                        $('#captured_image_modal').modal();
                    }
                }
            });
        });
    }

    // Capture an image using bulb timer and show it
    function capture_timelapse_image(takes, takeseconds, current) {
        // Increment current timelapse take
        current++;

        console.log('Capturing take ' + current + '/' + takes + ' of ' + takeseconds + ' seconds');
        $('#captured_image_status').text('Capturing take ' + current + '/' + takes)
        var data = {
            'seconds': takeseconds,
        }

        $.ajax({
            type: 'POST',
            url: '/image/capture_bulb/',
            data: data,
            dataType: 'json',
            success: function(response) {
                if (response.imagedata != null) {
                    $('#captured_image_status').text('Loading image ' + current + '/' + takes)
                    $('#captured_image')
                        .attr('src', 'data:image/jpg;base64,' + response.imagedata)
                }
                // Do next take if all takes are done
                if (current < takes) {
                    capture_timelapse_image(takes, takeseconds, current);
                } else {
                    $('#captured_image_status').text('Timelapse finished: ' + current + '/' + takes + ' takes done')
                }
            }
        });
    }

    function start_timelapse() {
        // Set current configuration
        set_config(function(){
            console.log('Starting timelapse');
            var takes = parseInt($('#takes').val());
            var takeseconds = parseInt($('#takeseconds').val());
            var current = 0;
            $('#timelapse_modal').modal();
            capture_timelapse_image(takes, takeseconds, current);
            $('#captured_image_modal').modal();
        });
    }

    function go_in_full_screen(selector) {
        element = $(selector).get(0);
        if(element.requestFullscreen)
            element.requestFullscreen();
        else if(element.mozRequestFullScreen)
            element.mozRequestFullScreen();
        else if(element.webkitRequestFullscreen)
            element.webkitRequestFullscreen();
        else if(element.msRequestFullscreen)
            element.msRequestFullscreen();
    }

    function toggle_advanced_mode() {
        if ($('#advanced_checkbox').prop('checked')) {
            $('.advanced').show();
        } else {
            $('.advanced').hide();
        }
        
    }

    // Event binding
    $('#advanced_checkbox').change(toggle_advanced_mode);
    $('#set_config').click(set_config);
    $('#capture_image').click(capture_image);
    $('#start_timelapse').click(start_timelapse);
    $('#capture_image_bulb').click(capture_image_bulb);
    $('#show_flats_screen').click(function(){
        go_in_full_screen('#flats_screen')
    })

    // Modal hiding
    $('#captured_image_modal').on('hidden.bs.modal', function (e) {
        $('#captured_image')
            .attr('src', GIF_BASE64_PIXEL)
    })

    // Hide advanced options
    toggle_advanced_mode()

    // Load current camera configuration
    load_config();
});
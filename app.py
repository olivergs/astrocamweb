from flask import Flask, request, render_template, send_from_directory, jsonify

from camcontrol import CamControl

app = Flask(__name__)

cctl = CamControl(debug=app.debug)

@app.route('/')
def index():
    return render_template('index.html')


@app.route('/config/', methods=['GET', 'POST'])
def config():
    if request.method == 'GET':
        return jsonify(cctl.get_config())

    if request.method == 'POST':
        return jsonify(cctl.set_config(request.form))


@app.route('/image/capture/', methods=['GET'])
def capture_image():
    return jsonify(cctl.capture_image())


@app.route('/image/capture_bulb/', methods=['POST'])
def capture_image_bulb():
    return jsonify(cctl.capture_image_bulb(int(request.form['seconds'])))


@app.route('/timelapse/start/', methods=['POST'])
def start_timelapse():
    return jsonify(cctl.start_timelapse(
        request.form['takes'], request.form['takeseconds']))


# Static data
@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('frontend/js', path)


@app.route('/css/<path:path>')
def send_css(path):
    return send_from_directory('frontend/css', path)
